# makefile for 'kerneltop'
# Randy Dunlap  <rddunlap@ieee.org>

all: kerneltop

DEBUG=
CC=gcc

ifeq "$(DEBUG)" "y"
CCFLAGS=-g
else
CCFLAGS=-s
endif

kerneltop: kerneltop.c
	$(CC) -c -O2 $(CCFLAGS) -I. -Wall -Wstrict-prototypes kerneltop.c -o kerneltop.o
	$(CC) $(CCFLAGS) kerneltop.o -o kerneltop

clean:
	rm -f kerneltop.o kerneltop

