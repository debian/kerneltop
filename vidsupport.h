// vidsupport.h:  video IO control support constants and inline functions
// Randy Dunlap  <rddunlap@ieee.org>

// ASCII character codes:
#define ESC		'\x1b'

/* see man(4) console_codes for info on the following: */

// video attributes:
#define ATTR_BOLD_ON		1
#define ATTR_DIM_ON		2
#define ATTR_UNDERLINE_ON	4
#define ATTR_BLINK_ON		5
#define ATTR_REVERSE_ON		7
#define ATTR_BOLD_OFF		22	// actually for NORMAL_INTENSITY
#define ATTR_DIM_OFF		22	// actually for NORMAL_INTENSITY
#define ATTR_UNDERLINE_OFF	24
#define ATTR_BLINK_OFF		25
#define ATTR_REVERSE_OFF	27

// colors:	// These are also passed as video attributes.
#define FG_BLACK		30
#define FG_RED			31
#define FG_GREEN		32
#define FG_BROWN		33
#define FG_BLUE			34
#define FG_MAGENTA		35
#define FG_CYAN			36
#define FG_WHITE		37
//
#define BG_BLACK		40
#define BG_RED			41
#define BG_GREEN		42
#define BG_BROWN		43
#define BG_BLUE			44
#define BG_MAGENTA		45
#define BG_CYAN			46
#define BG_WHITE		47
#define BG_DEFAULT		49

/* video row/column usage & formatting (app. space) */
/* rows & columns are 1-based */
#define MSG_STS_ROW		25

///////////////////// inline video routines /////////////////////////

inline void vidstr (int row, int col, char *str) {	// working
	printf ("%c[%d;%dH%s", ESC, row, col, str); }

inline void vid_putnow (void) {	// working
	fflush (stdout); }

inline void vid_attr (int attr) {	// working for attrs and colors
	printf ("%c[%dm", ESC, attr); }

inline void vid_palette (int color, int red, int green, int blue) { // not working
	printf ("%c]P%1.1x%2.2x%2.2x%2.2x", ESC, color, red, green, blue); }

inline void vid_palette_reset (void) {	// not working
	printf ("%c]R", ESC); }

inline void vid_clear_all (void) {	// working
	printf ("%c[2J", ESC); }

inline void vid_clear_curtoend (void) {	// working; erase from cursor to end of display
	printf ("%c[J", ESC); }

inline void vid_clear_starttocur (void) { // working; erase from start to cursor
	printf ("%c[1J", ESC); }

inline void vid_clear_line (void) {	// working; entire line
	printf ("%c[2K", ESC); }

inline void vid_clear_curtoeol (void) { // working; erase from cursor to end of line
	printf ("%c[K", ESC); }

inline void vid_clear_soltocur (void) { // working; erase from start of line to cursor
	printf ("%c[1K", ESC); }

inline void vid_curpos (int row, int col) {	// working
	printf ("%c[%d;%df", ESC, row, col); }

inline void vid_insert_spaces (int count) {	// working; note: curpos stays the same
	printf ("%c[%d@", ESC, count); }

inline void vid_insert_lines (int count) {	// working
	printf ("%c[%dL", ESC, count); }

inline void vid_delete_lines (int count) {	// working
	printf ("%c[%dM", ESC, count); }

inline void vid_delete_chars (int count) {	// working
	printf ("%c[%dP", ESC, count); }

inline void vid_erase_chars (int count) {	// working
	printf ("%c[%dX", ESC, count); }

// end vidsupport.h
